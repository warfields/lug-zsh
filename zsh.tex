\documentclass{lug}

\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}
\newcommand{\zshrc}{\texttt{\textasciitilde/.zshrc}}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\title{ZSH}
\author{Sumner Evans and Jordan Newport}
\institute{Mines Linux Users Group}

\begin{document}

\section{Basics}

\begin{frame}[fragile]{ZSH is a shell}
    ZSH is a UNIX shell with \texttt{bash}-like syntax, and many great features.
    \pause

    \textbf{And most of the people here at LUG use it.}
    \pause

    If you'd like to follow along with us during the talk, \texttt{zsh} is in
    all major distro repositories. To run it, just type: \texttt{zsh}.
    \pause

    If, at the end of this talk, you would like to change your shell to
    \texttt{zsh}, run the following command:

    \begin{minted}{bash}
        chsh -s `which zsh`
    \end{minted}
\end{frame}

\begin{frame}{Setting up ZSH}
    \textbf{Option 1}

    Use a framework like Oh My Zsh, or pretzo. Or a plugin manager like Antigen
    or zplug. This has the advantage of a plugin system and less time spent
    configuring.

    \textbf{Option 2}

    Do it yourself. Allows more customisation and typically lighter. You do this
    by editing your \zshrc, a script which runs when
    you start \texttt{zsh}.
\end{frame}

\section{Why ZSH > BASH}

\begin{frame}[fragile]{Extended Globbing}
    With 
    \begin{minted}{bash}
        setopt extendedglob
    \end{minted}
    \begin{itemize}
        \item \texttt{**} will recursively match subdirectories
        \item \texttt{***} will recursively match subdirectories \textit{while
                following symlinks}
    \end{itemize}
    \pause
    And with
    \begin{minted}{bash}
        setopt globstarshort
    \end{minted}
    you can shorten \texttt{**/*} to \texttt{**} and \texttt{***/*} to
    \texttt{***}. For example. \texttt{**.sh} would match all files ending in
    \texttt{.sh} recursively.  
\end{frame} 

\begin{frame}[fragile]{ZLE and Vim Mode}
        \Large
        {
            \setminted{fontsize=\Large,baselinestretch=1}
            \begin{minted}{bash}
                bindkey -v
            \end{minted}
        }
        will give you vim mode. Need I say more?

        \pause
        \vspace{1em}
        \normalsize
        \begin{minted}{bash}
            bindkey -e
        \end{minted}
    will give you emacs mode, if that's something you really want.

\end{frame} 

\begin{frame}[fragile]{Multiple redirection}
    \texttt{zsh} can redirect to and from multiple inputs/outputs at the same
    time. So...

    \medskip
    \small
    \begin{tabular}{l l}
        Rather than typing & You can type \\ \hline
        \texttt{w >file1; w >file2; w >file3} & \texttt{w >file\{1..3\}} \\
        \texttt{cat file\{1,2\} | less} & \texttt{less <file\{1,2\}} \\
        \texttt{./server | tee log | grep ERR} &
        \texttt{./server >log | grep ERR} \\
    \end{tabular}
\end{frame} 

\section{Essential Configs}

\begin{frame}[fragile]{Writing a \zshrc}

    Now that you have seen the glory of ZSH, and assuming you want to go with
    \textbf{Option 2}, here are some things you may consider adding to your
    \texttt{.zshrc}:
    \medskip

    \begin{tabular}{l l}
        \texttt{HISTFILE=\textasciitilde/.histfile} & Persistent history \\
        \texttt{HISTSIZE=10000} & Up to 10,000 items in history \\
        \texttt{SAVEHIST=10000} & Up to 10,000 items persistent \\
        \texttt{setopt appendhistory} & Append history to the history file \\
        \texttt{setopt histignoredups} & Ignore duplicates in history \\
        \texttt{setopt histignorespace} & Ignore lines which begin with a space \\
        \texttt{setopt autopushd} & Use the dirstack as you \texttt{cd}
    \end{tabular}

    \medskip

    Also don't forget to set your \texttt{EDITOR}, \texttt{PAGER}, etc.

    \mintinline{bash}{export EDITOR=nvim} for example.

\end{frame}

\begin{frame}
    \frametitle{More \texttt{setopt} options}
    \begin{itemize}
        \item \texttt{beep}/\texttt{nobeep} - Ring the terminal bell on
            \texttt{zle} error
        \item \texttt{notify} --- Report the status of background jobs
            immediately
        \item \texttt{nomatch} --- If a globbing pattern has no matches, print
            an error, instead of leaving it unchanged in the argument list.
        \item \texttt{autocd} --- Change to a directory if you just type the name
        \item \texttt{correct} --- Typo Correction
    \end{itemize}

    Read \texttt{man zshoptions}. There are many options.

\end{frame}

\begin{frame}[fragile]{Searching for previous commands}
    Want to search for a previous command?
    \begin{minted}{bash}
        bindkey "^R" history-incremental-search-backward
    \end{minted}
    binds \texttt{ctrl+r} to search for previous commands.
    No more hitting the up arrow 10,000 times!
\end{frame}

\begin{frame}[fragile]{Directory hashes}
    \begin{minted}{bash}
        hash -d foo=~/bar/baz
    \end{minted}
    lets you \texttt{cd foo} to \texttt{cd} to \texttt{\textasciitilde/bar/baz}.

    \pause
    These can be recursive. For example, after that line you could add
    \begin{minted}{bash}
        hash -d doc=~foo/Documents
    \end{minted}
    which lets you \texttt{cd doc} to \texttt{cd} to \texttt{\textasciitilde/bar/baz/Documents}.
\end{frame}

\begin{frame}[fragile]{Push-line}
    Have you ever wanted to run a different command before the one you're
    typing now? Bind a key to \texttt{push-line}! I use q in vim normal mode:
    \begin{minted}{bash}
        bindkey -M vicmd q push-line
    \end{minted}
    Then just press \texttt{<Esc>q} when you have another command to run first,
    and your old command will reappear after the first one finishes.

\end{frame}

\section{Quality of Life Configs}

\begin{frame}[fragile]{Autocompletion}
    To add intelligent completion to \texttt{zsh}, add this line to your
    \zshrc:
    \begin{minted}{bash}
        autoload -U compinit && compinit
    \end{minted}
    \pause

    to automatically rehash completions, add:
    \begin{minted}{bash}
        zstyle ':completion:*' rehash true
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Useful Autocompletion Configs}
    There are a lot of options for autocompletion. For example:
    \medskip
    \pause

    \textbf{Case Correction:}
    \begin{minted}{bash}
        zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
    \end{minted}
    \medskip
    \pause

    \textbf{Approximate Correction:} this allows for $n/3$ errors per $n$
    characters typed.
    \begin{minted}{bash}
        zstyle ':completion:*:approximate:' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3 )) numeric)'
    \end{minted}
    \pause

    There are a bunch more configuration options, and I don't know what a lot of
    the ones in my config do, but they work pretty well for me!

    One of them lets you autocomplete things like man pages, which is incredibly
    convenient.
\end{frame}

\begin{frame}{Aliases}
    Aliases allow you to type something easy to do something more complex. There
    are three main alias types that we care about:

    \begin{itemize}
        \item \textbf{Command aliases:} alias one command to another command.
        \item \textbf{Global aliases:} expand anywhere in the line.
        \item \textbf{Suffix aliases:} allow you to specify what program to use
            to open a particular type of file.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Aliases: Command Aliases}
    The syntax is simple:
    \begin{minted}{bash}
        alias name="the long command"
    \end{minted}
    \medskip
    \pause

    Here are a couple of useful command aliases:
    \begin{minted}{bash}
        alias alpr='ssh isengard lpr -P bb136-printer -o coallate=true'
        alias alprd='ssh isengard lpr -P bb136-printer -o coallate=true -o Duplex=DuplexNoTumble'
        alias gl="git log --pretty=format:'%C(auto)%h %ad %C(green)%s%Creset %C(auto)%d [%an (%G? %GK)]' --graph --date=format:'%Y-%m-%d %H:%M' --all"
        alias antioffice='libreoffice --headless --convert-to pdf'
        alias hostdir="python -m http.server"
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Aliases: Global Aliases}
    The syntax is very similar to command aliases:
    \begin{minted}{bash}
        alias -g name="the long command"
    \end{minted}
    \medskip

    Here are a couple of useful global aliases:
    \begin{minted}{bash}
        alias -g ...='../..'
        alias -g ....='../../..'
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Aliases: Suffix Aliases}
    The syntax is, as you might expect by now, very similar to command and
    global aliases:
    \begin{minted}{bash}
        alias -s fileext="the command to use"
    \end{minted}
    \medskip

    Here are a couple of useful suffix aliases:
    \begin{minted}{bash}
        alias -s mp4=mpv
        alias -s tex=nvim
        alias -s doc=libreoffice
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Custom Everything!}
    In ZSH, you can customize almost everything. Here's an example of how you
    can customize your prompt:
    \begin{minted}{bash}
        autoload -Uz promptinit && promptinit
        prompt adam1
        function __rprompt {
          # Capture result of last command.
          if [[ $? -eq 0 ]]; then
            RESULT="[%{$fg_no_bold[green]%}%?%{$reset_color%}]"
          else
            RESULT="[%{$fg_bold[red]%}%?%{$reset_color%}]"
          fi
          echo -n $RESULT
        }

        export RPS1='$(__rprompt)'
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Custom Key Widgets}
    Write a function, then bind it with \texttt{zle -N}. Example:
    \begin{minted}{bash}
        function __zkey_prepend_sudo {
            if [[ $BUFFER != "sudo "*  ]]; then
                BUFFER="sudo $BUFFER"
                CURSOR+=5
            else
                BUFFER="${BUFFER:5}"
            fi
        }
        zle -N prepend-sudo __zkey_prepend_sudo
        bindkey -M vicmd "s" prepend-sudo
    \end{minted}
    Now \texttt{<Esc>s} will toggle ``\texttt{sudo }'' at the beginning of the
    command.
\end{frame}

\begin{frame}[fragile]{Custom Functions}
    In your {\zshrc}, you can define utility functions such as this:
    \begin{minted}{bash}
        # Use the https://gitignore.io API to retrieve gitignores
        function wgitignore() {
            ignores=$(printf ",%s" "$*[@]")
            ignores=${ignores:1}
            wget "https://www.gitignore.io/api/${ignores}" -O - >> .gitignore
        }
    \end{minted}

    Now, \texttt{wgitignore cpp} will download the auto-created
    \texttt{.gitignore} for C++ using the \url{https://gitignore.io} API.
\end{frame}

\begin{frame}[fragile]{Custom Actions after \texttt{cd}}
    If you are anything like me, you always \texttt{ls} after \texttt{cd}. You
    can define the \texttt{chpwd} function in your \zshrc.
    \begin{minted}{bash}
        function chpwd() {
            emulate -L zsh
            la
        }
    \end{minted}

    I have a bunch more stuff in my \texttt{chpwd} function such as activating
    Python virtual environments and auto-fetching from git.
\end{frame}

\begin{frame}[fragile]{Up Arrow Search}
    Add the following to your \zshrc:
    \begin{minted}{bash}
        autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
        zle -N up-line-or-beginning-search
        zle -N down-line-or-beginning-search
        bindkey "^[[A" up-line-or-beginning-search # Up
        bindkey "^[[B" down-line-or-beginning-search # Down
        bindkey -M vicmd "k" up-line-or-beginning-search
        bindkey -M vicmd "j" down-line-or-beginning-search
    \end{minted}
    then, you can type \texttt{vim} and then \texttt{<Esc>\textuparrow} will
    only go back in history to commands which start with \texttt{vim}.
\end{frame}

\begin{frame}{Awesome ZSH Plugins}
    There are a bunch of cool plugins for ZSH. Here are a couple of cool ones
    that I use:
    \begin{itemize}
        \item \href{https://github.com/zsh-users/zsh-syntax-highlighting}{
                \url{github.com/zsh-users/zsh-syntax-highlighting}}
        \item \href{https://github.com/zsh-users/zsh-autosuggestions}{
                \url{github.com/zsh-users/zsh-autosuggestions}}
        \item \href{https://github.com/MichaelAquilina/zsh-you-should-use}{
                \url{github.com/MichaelAquilina/zsh-you-should-use}}
        \item Command not found (part of \texttt{pkgfile}):
            \texttt{/usr/share/doc/pkgfile/command-not-found.zsh}
    \end{itemize}
\end{frame}

\begin{frame}
    \Huge
    Questions?
\end{frame}

\begin{frame}{Resources}
    \begin{itemize}
        \item Sumner's ZSH config:
            \href{https://gitlab.com/sumner/dotfiles/blob/master/.zshrc}{
                \url{gitlab.com/sumner/dotfiles/blob/master/.zshrc}}
        \item Jack R.'s full-length ZSH presentation:
            \href{https://github.com/jackrosenthal/lug-zsh-presentation/blob/master/zsh.pdf}{
                \url{github.com/jackrosenthal/lug-zsh-presentation/blob/master/zsh.pdf}}
    \end{itemize}
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
