LUG zsh presentation
====================

:Author: Sumner Evans and Jordan Newport

Topics Covered
--------------

Basics

- mention Oh My Zsh and how much it sucks
- how to change shell

Cool set-apart features

- Extended Glob
- vim mode
- multiple redirection

Things you need to survive

- basic useful zsh configs
- get ctrl + r
- directory hashes
- pushline

QOL

- Autocompletion

  - approximate
  - ignore caps

- Aliases

  - global aliases
  - suffix aliases

- All of the customization

  - custom key widgets
  - custom functions
  - custom things after ``cd``

- up arrow search
- fun plugins:

  - syntax highlighting
  - autosuggestions
  - you should use
  - command not found
